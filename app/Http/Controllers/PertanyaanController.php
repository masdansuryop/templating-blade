<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Pertanyaan;
use Auth;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $validationData = $request->validate([
            'judul' => 'required|max:255'
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'tanggal_dibuat' => Carbon::now(),
        // ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->tanggal_dibuat = Carbon::now();
        // $pertanyaan->save();

        $pertanyaan = Pertanyaan::create([
            'judul' => $request['judul'],
            'tanggal_dibuat' => Carbon::now()
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function index()
    {
        // $pertanyaan = DB::table('pertanyaan')->get();

        $pertanyaan = Pertanyaan::all();

        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $validationData = $request->validate([
            'judul' => 'required|max:255'
        ]);

        // $query = DB::table('pertanyaan')
        //             ->where('id', $id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'tanggal_diperbaharui' => Carbon::now(),
        //             ]);

        $pertanyaan = Pertanyaan::where('id', $id)->update([
            'judul' => $request['judul'],
            'tanggal_diperbaharui' => Carbon::now(),
            'profil_id' => Auth::id()
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil diupdate!');
    }

    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();

        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
